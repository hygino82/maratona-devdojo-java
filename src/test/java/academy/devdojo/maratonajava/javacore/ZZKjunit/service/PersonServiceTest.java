package academy.devdojo.maratonajava.javacore.ZZKjunit.service;

import academy.devdojo.maratonajava.javacore.ZZKjunit.dominio.Person;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;


class PersonServiceTest {

    private Person adult;
    private Person notAdult;
    private PersonService personService;

    @BeforeEach
    public void setUp() {
        adult = new Person(18);
        notAdult = new Person(15);
        personService = new PersonService();
    }

    @Test
    @DisplayName("Deve retornar falso quando a idade é menor que 18")
    void isAdult_ReturnFalse_WhenAgeIsLowerThan18() {
        Assertions.assertFalse(personService.isAdult(notAdult));
    }

    @Test
    @DisplayName("Deve retornar verdadeiro quando a idade é maior ou igual a 18")
    void isAdult_ReturnTrue_WhenAgeIsGreaterOrEqualsThan18() {
        Assertions.assertTrue(personService.isAdult(adult));
    }

    @Test
    @DisplayName("Deve lançar uma excessão quando o objeto é nulo")
    void isAdult_ShouldThrowException_WhenPersonIsNull() {
        String msg = "Person can't be null";
        Assertions.assertThrows(IllegalArgumentException.class, () -> personService.isAdult(null), msg);
    }

    @Test
    @DisplayName("Deve retornar a lista contendo apenas dois adultos")
    void filterRemovingNotAdult_ShouldReturnListWithAdultOnlyWhenListOfPersonWithAdultPassed() {
        Person person1 = new Person(17);
        Person person2 = new Person(18);
        Person person3 = new Person(21);
        List<Person> personList = List.of(person1, person2, person3);

        Assertions.assertEquals(2, personService.filterRemovingNotAdult(personList).size());
    }
}