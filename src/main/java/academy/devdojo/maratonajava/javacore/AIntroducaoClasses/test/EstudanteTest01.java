package academy.devdojo.maratonajava.javacore.AIntroducaoClasses.test;

import academy.devdojo.maratonajava.javacore.AIntroducaoClasses.dominio.Estudante;

public class EstudanteTest01 {
    public static void main(String[] args) {
        Estudante estudante = new Estudante("Luffy", 21, 'M');
        System.out.println(estudante);
    }
}
