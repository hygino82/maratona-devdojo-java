package academy.devdojo.maratonajava.javacore.ZZKjunit.dominio;
import lombok.*;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Person {
    private int age;
}
