package academy.devdojo.maratonajava.introducao;

public class Aula02TiposPrimitivos {
    public static void main(String[] args) {
        int valorInteiro = 3;
        boolean valorBoleano = true;
        float valorFloat = 3.478f;
        double valorDouble = 3147.145;
        long valorLong = 641L;
        char valorChar = 'w';
        byte bitValor = (byte) 0b1010010;
        short valorShort = 21458;
        System.out.println("O valor inteiro é " + valorInteiro);
    }
}
